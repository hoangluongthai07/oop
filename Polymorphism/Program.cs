﻿// khởi tạo A
A a = new A();

IMayTinh tong = new TinhTong();
var r1 = a.Calculated(tong, 2, 4); // truyền cách tính tổng vào hàm
Console.WriteLine(r1);

IMayTinh tru = new PhepTru();
var r2 = a.Calculated(tru, 7, 4); // truyền cách tính hiệu vào hàm
Console.WriteLine(r2);

// Kết luận: Polymorphisum giúp hàm Calculated trở nên dễ dàng mở rộng nhiều cách tính(đa hình)

class A
{
    public int Calculated(IMayTinh mayTinh, int x, int y)
    {
        // Các đối tượng thuộc các lớp khác nhau (lớp con) có thể được xử lý bằng cách thức chung (lớp cha). 
        // Dễ dàng quản lý mã nguồn. Linh hoạt trong việc mở rộng.
        int result = mayTinh.PhepTinhVoi2So(x, y);
        return result;
    }
}

interface IMayTinh
{
    int PhepTinhVoi2So(int a, int b);
}

class TinhTong : IMayTinh
{
    public int PhepTinhVoi2So(int a, int b)
    {
        return a + b;
    }
}
class PhepTru : IMayTinh
{
    public int PhepTinhVoi2So(int a, int b)
    {
        return a - b;
    }
}

